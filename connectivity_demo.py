import json
import psycopg2
import urllib.request as urllib
import xmldict

from multiprocessing import Pool
from optparse import OptionParser
from psycopg2.extras import Json


DB_USER = "*"
DB_PW = "*"
DB_HOST = "*"
DB_PORT = "*"
DB_DATABASE = "*"


def send_post(url, body, input_type):
    try:
        if input_type == "json":
            data_as_bytes = json.dumps(body).encode()
        else:
            data_as_bytes = body.encode()

        req = urllib.Request(
            url=url,
            data=data_as_bytes,
            headers={
                'Content-Type': 'application/{0}; charset=utf-8'.format(input_type),
                'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36'
            }
        )
        response = urllib.urlopen(req)
        data = response.read()
        response.close()

        return parse_response(data, input_type)
    except Exception as ex:
        return str(ex)


def get_options():
    optionParser = OptionParser()
    optionParser.add_option("--city_string_input",
                            help="City string",
                            action="store",
                            default="city_string_input")
    optionParser.add_option("--checkin_string_input",
                            help="Checkin string",
                            action="store",
                            default="checkin_string_input")
    optionParser.add_option("--checkout_string_input",
                            help="Checkout string",
                            action="store",
                            default="checkout_string_input")
    options, _ = optionParser.parse_args()

    return options


def parse_response(response, type):
    if type == "json":
        return json.loads(response)
    else:
        return xmldict.xml_to_dict(response.decode('utf-8'))


def initialize_db():
    connection = psycopg2.connect(user = DB_USER,
                                  password = DB_PW,
                                  host = DB_HOST,
                                  port = DB_PORT,
                                  database = DB_DATABASE)
    connection.autocommit = True
    cursor = connection.cursor()
    
    cursor.execute("DROP TABLE hotels;")
    cursor.execute("CREATE TABLE hotels (ID INTEGER NOT NULL PRIMARY KEY, info jsonb NOT NULL);")

    return cursor


def get_common_hotels(json_hotels, xml_hotels):
    xml_hotels_infos = {}
    for hotel in xml_hotels:
        xml_hotels_infos[hotel['id']] = float(hotel['price'])

    common_hotels = []
    for hotel in json_hotels:
        curr_id = str(hotel['id'])
        if curr_id in xml_hotels_infos:
            hotel['prices'] = {
                'snaptravel': hotel['price'],
                'retail': xml_hotels_infos[curr_id]
            }
            del(hotel['price'])
            common_hotels.append(hotel)

    return common_hotels


def main():
    # Parse arguments from command line.
    # python test.py --city_string_input="city_string_input" --checkin_string_input="checkin_string_input" --checkout_string_input="checkout_string_input"
    options = get_options()

    json_body = {
      "city" : options.city_string_input,
      "checkin" : options.checkin_string_input,
      "checkout" : options.checkout_string_input,
      "provider" : 'snaptravel',
    }
    xml_body = """<?xml version="1.0" encoding="UTF-8"?>
    <root>
      <checkin>{checkin_string_input}</checkin>
      <checkout>{checkout_string_input}</checkout>
      <city>{city_string_input}</city>
      <provider>snaptravel</provider>
    </root>""".format(checkin_string_input=options.city_string_input, checkout_string_input=options.checkout_string_input, city_string_input=options.city_string_input)

    # Get response from endpoints in parallel
    pool = Pool()
    result1 = pool.apply_async(send_post, ["https://experimentation.getsnaptravel.com/interview/hotels", json_body, "json"])
    result2 = pool.apply_async(send_post, ["https://experimentation.getsnaptravel.com/interview/legacy_hotels", xml_body, "xml"])
    json_request = result1.get()
    xml_request = result2.get()

    # Find common hotels & save prices
    common_hotels = get_common_hotels(json_request["hotels"], xml_request["root"]["element"])

    # Initialize connection & set up db
    db_cursor = initialize_db()

    # Save data to db
    for hotel in common_hotels:
        db_cursor.execute('insert into hotels (id, info) VALUES (%s, %s)', [hotel['id'], Json(hotel)])

    # Print saved data
    db_cursor.execute("SELECT * FROM hotels")
    records = db_cursor.fetchall()
    for a in records:
        print(a)


if __name__ == "__main__":
    main()
